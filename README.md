# Filter-collection

Install via npm:
`npm i filter-collection`

Install via yarn:
`yarn add filter-collection`

## Usage

`const { drop, keep } = require('filter-collection');`

### drop(sourceCollection, keysToDrop)

Drop selected keys from result collection

    const sourceCollection = [
      { id: 1, name: 'John', age: 13, hobby: ['football', 'socker', 'PC'] },
      { id: 2, name: 'Ian', age: 23, hobby: ['football', 'graffity'] },
      { id: 3, name: 'Roman', age: 48, hobby: ['snowboard', 'PC'] },
    ];
    const keysToDrop = ['id','hobby'];
    const result = drop(sourceCollection, keysToDrop);
    console.log(result);
    /*
    [{ name: 'John', age: 13 },
      { name: 'Ian', age: 23 },
      { name: 'Roman', age: 48 }]
    */

### keep(sourceCollection, keysToKeep)

Keep selected keys in result collection

    const sourceCollection = [
      { id: 1, name: 'John', age: 13, hobby: ['football', 'socker', 'PC'] },
      { id: 2, name: 'Ian', age: 23, hobby: ['football', 'graffity'] },
      { id: 3, name: 'Roman', age: 48, hobby: ['snowboard', 'PC'] },
    ];
    const keysToKeep = ['id', 'name'];
    const result = keep(sourceCollection, keysToKeep);
    console.log(result);
    /*
    [{ id: 1, name: 'John' },
      { id: 2, name: 'Ian'},
      { id: 3, name: 'Roman' }]
    */
